const express = require("express")
const app = express()
const http = require("http")
const server = http.createServer(app)
const SocketIO = require("socket.io").Server

app.use("/", express.static("public"))

const io = new SocketIO(server)

io.on("connection", (socket) => {
    console.log(`new socket io connected: ${socket.id}`)

    let username = "Anonymous"
    socket.on("register", (data) => {
        username = data
    })

    // chat event...
    socket.on("send.chat", text => {
        let chatData = {
            text: text,
            date: new Date().toLocaleTimeString(),
            username: username,
        }
        console.log(chatData)
        socket.broadcast.emit("chat.receive", chatData)
        socket.emit("chat.sent", chatData)
    })

    socket.on("disconnect", () => {
        console.log(`disconnected client: ${socket.id}`)
    })
})

server.listen(8000, () => {
    console.log(`server running on port 8000`)
})
